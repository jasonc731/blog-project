class AddRatingToArticles < ActiveRecord::Migration
  def change
    # adds another column to articles
    add_column :articles, :rating, :integer
  end
end

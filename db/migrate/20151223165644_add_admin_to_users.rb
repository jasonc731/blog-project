class AddAdminToUsers < ActiveRecord::Migration
  def change
    # by default this is false
    add_column :users, :admin, :boolean, default: false
  end
end

require 'test_helper'

class CreateCategoriesTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = User.create(username: "john", email: "exmaple@gmail.com", password: "password", admin: true)
  end

  test "get new category form and create category" do
    sign_in_as(@user, "password")
    get new_category_path # going to new path
    assert_template 'categories/new' # rendering template for new path

    # posting it as category = sports
    assert_difference 'Category.count', 1 do
      post categories_path, category: {name: "sports"}
    end
    assert_template 'categories/index' # redirecting to index page
    assert_match "sports", response.body # matches with category and shows it on the main page (index)
  end

  test "invalid category submission results in failure" do
    sign_in_as(@user, "password")
    get new_category_path # goes to the new path
    assert_template 'categories/new' # renders the new page

    # shows not difference here, and posts the the categories path (create)
    assert_no_difference 'Category.count' do
      post categories_path, category: {name: " "}
    end
    assert_template 'categories/new' # renders new template
  end

end

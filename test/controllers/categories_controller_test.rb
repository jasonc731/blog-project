require 'test_helper'

class CategoriesControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @category = Category.create(name: "sports")
    @user = User.create(username: "john", email: "exmaple@gmail.com", password: "password", admin: true)
  end

  test "should get categories index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    session[:user_id] = @user.id
    get :new
    assert_response :success
  end

  # shows category based on ID
  test "should get show" do
    get(:show, {'id' => @category.id}) # shows id for that category
    assert_response :success
  end

  # admin needs to be logged in
  test "should redirect create when admin not logged in" do
    assert_no_difference 'Category.count' do
      post :create, category: {name: "sports"}
    end
    assert_redirected_to categories_path
  end

end

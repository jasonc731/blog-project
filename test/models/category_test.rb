require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  # THESE ARE ALL BASED OFF OF WHAT IS IN THE MODEL

  # this runs every time you run a test
  def setup
    @category = Category.new(name: "sports")
  end

  # checking if the category is valid
  test "category should be valid" do
    assert @category.valid? # is it valid
  end

  # checks if name is present
  test "name should be present" do
    @category.name = " "
    assert_not @category.valid?
  end

  # forces name to be unique
  test "name should be unique" do
    @category.save
    category2 = Category.new(name: "sports")
    assert_not category2.valid?
  end

  # max is 25
  test "name should not be too long" do
    @category.name = "a" * 26
    assert_not @category.valid?
  end

  # min is 5
  test "name should not be too short" do
    @category.name = "aaaa"
    assert_not @category.valid?
  end

end

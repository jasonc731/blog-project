class Article < ActiveRecord::Base
  belongs_to :user
  has_many :article_categories # has many article categories
  has_many :categories, through: :article_categories # has many categories
  validates :title, presence: true, length: {minimum: 3, maximum: 30} # validation for title, checks length
  validates :description, presence: true, length: {minimum: 10, maximum: 200} # validation for description, checks length
  validates :user_id, presence: true
end

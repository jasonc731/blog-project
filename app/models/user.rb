class User < ActiveRecord::Base
  has_secure_password

  # Destroys all articles if they are deleted
  has_many :articles, dependent: :destroy
  before_save { self.email = email.downcase}
  # ensures that user is unique
  validates :username, presence: true, uniqueness: {case_sensitive: false},length: {minimum: 5, maximum: 25}
  VALID_EMAIL_REGEX= /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :email, presence: true, uniqueness: {case_sensitive: false}, format: {with: VALID_EMAIL_REGEX}
end

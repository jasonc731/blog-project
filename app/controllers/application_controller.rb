class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # available to views
  helper_method :current_user, :logged_in?

  # Find user in DB for that user, then return the user
  # if not the current user then find the user
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  # Boolean for current user (!!)
  def logged_in?
    !!current_user
  end

  def require_user
    if !logged_in?
      flash[:danger] = "You must be logged in to perform that action."
      redirect_to root_path
    end
  end

end

class ArticlesController < ApplicationController

  # this lets the class know to run the set_article_id method for this other methods. Pretty much calling it
  before_action :set_article_id, only: [:edit, :update, :show, :destroy]

  # Require user to do tasks
  before_action :require_user, except: [:index, :show]

  #Require the same user
  before_action :require_same_user, only: [:edit, :update, :destroy]

  # shows all articles
  def index
    @articles = Article.paginate(page: params[:page], :per_page => 5)
  end

  # adds new article to DB
  def new
      @article = Article.new
  end

  def edit
    #@article = Article.find(params[:id])
  end

  def update
    #@article = Article.find(params[:id])
    if @article.update(article_params)
      flash[:success] = "Article was updated."
      redirect_to user_path(current_user)
    else
      render 'edit'
    end
  end

  # creating the articles and redirecting to main page
  def create
    # render plain: params[:article] shows what is being passed
    @article = Article.new(article_params)
    @article.user = current_user

    if @article.save
      flash[:success] = "Article was successfully created."
      redirect_to user_path(current_user)
    else
      render 'new'
    end

  end

  # showing the article based on the id
  def show
    #@article = Article.find(params[:id]) # search for article based on ID
  end

  # deletes article based on id number
  def destroy
    #@article = Article.find(params[:id])
    @article.destroy
    flash[:danger] = "Article was deleted."
    redirect_to articles_path
  end

  # private definition that allows the views to show these attributes
  private

  # setting article ID
  def set_article_id
    @article = Article.find(params[:id])
  end

  # shows all article params on page when referencing
  def article_params
    params.require(:article).permit(:title, :description, :rating, category_ids: []) # -> adding category ids as an array
  end

  def require_same_user
    if current_user != @article.user and !current_user.admin?
      flash[:danger] = "You can only delete or edit your own articles."
      redirect_to root_path
    end
  end

end
